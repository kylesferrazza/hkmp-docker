FROM mono:6.12
COPY server.exe /server.exe
EXPOSE 26950
ENTRYPOINT ["mono", "/server.exe", "26950"]
